import numpy as np

# 2D numpy array
a = np.array([(1, 2, 3), (4, 5, 6)])

# sum of axis 0 : columns
print(a.sum(axis=0))

# sum of axis 1 : rows
print(a.sum(axis=1))