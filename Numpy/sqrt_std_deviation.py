import numpy as np

a = np.array([(1, 2, 3), (4, 5, 6)])
# np.sqrt(arr) gives square root of all the elements
print(np.sqrt(a))
# np.std(arr) gives standard deviation of all the elements of the array.
print(np.std(a))
