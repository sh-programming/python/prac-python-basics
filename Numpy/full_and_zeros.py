import numpy as np

# full function will fill a n-dimensional array with identical values
print(np.full((2,4),5))
# zeros function will fill a n-dimensional array with zeros
print(np.zeros((3,6)))
