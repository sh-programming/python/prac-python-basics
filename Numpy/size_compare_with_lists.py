import numpy as np
import time
import sys

# Takes all integer values from 0 to 1000 and give it to variable S
S = range(1000)
# sys.getsizeof(element) returns the size of element and len(nameOfList) returns length of list
print(sys.getsizeof(5) * len(S))

# arange() will return values in a particular range.
# If starting and incrementing value is not specified it will be taken as 0 and 1 respectively.
D = np.arange(1000)
# numpyArray.size gives number of elements in the array.
# numpyArray.itemsize gives the size of one element in the array.
print(D.size * D.itemsize)
print(D)
