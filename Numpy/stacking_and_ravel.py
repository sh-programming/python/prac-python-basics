import numpy as np

a = np.array([(1, 2, 3), (4, 5, 6)])
b = np.array([(2, 2, 2), (4, 4, 4)])

# stacking is arranging arrays side by side or vertically

# vertical stacking
print(np.vstack((a,b)))

# horizontal stacking
print(np.hstack((a,b)))

# to print numpy array in single line
print(np.ravel(a))